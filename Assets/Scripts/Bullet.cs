﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Tower towerPrefab;
    private void Awake()
    {
        StartCoroutine(LifeCycle());
        GetComponent<Rigidbody2D>().velocity = transform.up;
    }

    private IEnumerator LifeCycle()
    {
        int life = Random.Range(1, 4);
        yield return new WaitForSeconds(life);
        Die();
        
    }

    private void Die()
    {
        Instantiate(towerPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Destroyed");
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}
