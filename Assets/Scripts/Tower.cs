﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private Bullet bullet;
    [SerializeField] private Transform spawnPoint;
    void Start()
    {
        StartCoroutine(Rotate());
    }
    

    private IEnumerator Rotate()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            float rotation = Random.Range(15, 45);
            transform.rotation *= Quaternion.Euler(0, 0,rotation);
            Shoot();
        }
    }

    private void Shoot()
    {
        Instantiate(bullet, spawnPoint.transform.position, spawnPoint.rotation);
    }
    
}
